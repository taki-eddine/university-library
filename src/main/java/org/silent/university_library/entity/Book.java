package org.silent.university_library.entity;

////////////////////////////////////////////////////////////////////////////////
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.silent.university_library.dao.CopyDAO;
////////////////////////////////////////////////////////////////////////////////

public class Book {

    public static final String COLUMN_ID          = "id";
    public static final String COLUMN_TITLE       = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_IMAGE       = "image";
    public static final String COLUMN_CATEGORY_ID = "category_id";

    public static final String COLUMNS_NAMES =
        COLUMN_ID          + "," +
        COLUMN_TITLE       + "," +
        COLUMN_DESCRIPTION + "," +
        COLUMN_IMAGE       + "," +
        COLUMN_CATEGORY_ID;

    public static final String[] COLUMNS_NAMES_ARRAY = {
        COLUMN_ID,
        COLUMN_TITLE,
        COLUMN_DESCRIPTION,
        COLUMN_IMAGE,
        COLUMN_CATEGORY_ID
    };

    ////////////////////////////////////////
    /*
    public static final Map<String, DBColumn> COLUMNS = new HashMap<>();

    static {
        COLUMNS.put(COLUMN_ID, new DBColumn<Book, Integer>((pair) -> {
            pair.getKey().setId(pair.getValue());
            return null;
        }));

        COLUMNS.put(COLUMN_ID, new DBColumn<Book, String>( (pair) -> {
            pair.getKey().setTitle(pair.getValue());
            return null;
        }));

        COLUMNS.put(COLUMN_DESCRIPTION, new DBColumn<Book, String>( (pair) -> {
            pair.getKey().setDescription(pair.getValue());
            return null;
        }));

        COLUMNS.put(COLUMN_IMAGE, new DBColumn<Book, byte[]>( (pair) -> {
            pair.getKey().setImage(pair.getValue());
            return null;
        }));

        COLUMNS.put(COLUMN_CATEGORY_ID, new DBColumn<Book, Long>( (pair) -> {
            pair.getKey().setCategoryId(pair.getValue());
            return null;
        }));
    }*/

    @Getter @Setter private long   id          = 0;
    @Getter @Setter private String title       = null;
    @Getter @Setter private String description = null;
    @Getter @Setter private byte[] image       = null;
    @Getter @Setter private long   categoryId  = 0L;

    // TODO: add custom Getter for category to load the data from the database.
    @Getter @Setter private Category category = null;
    @Setter private List<Copy>       ownCopys = null;

    ////////////////////////////////////////
    public List<Copy> getOwnCopys() {
        return CopyDAO.findByBookId(id);
    }

}
