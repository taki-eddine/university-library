package org.silent.university_library.entity;

////////////////////////////////////////////////////////////////////////////////
import lombok.Getter;
import lombok.Setter;
////////////////////////////////////////////////////////////////////////////////

public class Copy {

    @Getter @Setter private long    id         = 0L;
    @Getter @Setter private boolean disponible = false;
    @Getter @Setter private Book    book       = null;
    @Getter @Setter private Type    type       = null;

}
