package org.silent.university_library.entity;

////////////////////////////////////////////////////////////////////////////////
import lombok.Getter;
import lombok.Setter;
////////////////////////////////////////////////////////////////////////////////

public class Student {

    @Getter @Setter private Integer id       = 0;
    @Getter @Setter private String  name     = null;
    @Getter @Setter private String  lastName = null;

    @Getter @Setter private String userName = null;
    @Getter @Setter private String password = null;

    ////////////////////////////////////////
    @Override
    public String toString() {
        return "Student[ id = " + id + ", name = " + name + " ]";
    }
}
