package org.silent.university_library.entity;

////////////////////////////////////////////////////////////////////////////////
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
////////////////////////////////////////////////////////////////////////////////

public class Type {

    @Getter @Setter private long       id       = 0L;
    @Getter @Setter private String     name     = null;
    @Getter @Setter private List<Copy> ownCopys = null;

    ////////////////////////////////////////
    public static Type parseType(ResultSet input) throws SQLException {
        Type type = new Type();

        type.setId(input.getLong("id"));
        type.setName(input.getString("name"));

        return type;
    }

}
