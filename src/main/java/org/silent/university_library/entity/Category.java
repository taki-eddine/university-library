package org.silent.university_library.entity;

////////////////////////////////////////////////////////////////////////////////
import java.util.List;
import lombok.Getter;
import lombok.Setter;
////////////////////////////////////////////////////////////////////////////////

public class Category {

    @Getter @Setter private long   id       = 0L;
    @Getter @Setter private String name     = null;

    @Setter private List<Book> ownBooks = null;

    ////////////////////////////////////////
    public List<Book> getOwnBooks() throws Exception {
        // TODO: Add function logic.
        throw new Exception("Missing function logic");
    }

}
