package org.silent.university_library.dao;

////////////////////////////////////////////////////////////////////////////////
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import org.silent.university_library.commun.ConnectionDB;
import org.silent.university_library.entity.Student;
////////////////////////////////////////////////////////////////////////////////

public class StudentDAO {
    ////////////////////////////////////////
    /// \return Strudent.
    ///
    private static Student parseStudent(ResultSet input) throws SQLException {
        Student student = new Student();

        student.setId(input.getInt("id"));
        student.setName(input.getString("name"));
        student.setLastName(input.getString("last_name"));
        student.setUserName(input.getString("user_name"));
        student.setPassword(input.getString("password"));
        return student;
    }

    ////////////////////////////////////////
    /// \return All students.
    ///
    public static List<Student> findAll() {
        ConnectionDB  connectionDB = new ConnectionDB();
        List<Student> list         = new ArrayList<>();

        try(Connection        connection        = connectionDB.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT `id`, `name`, `last_name`, `user_name`, `passowrd` " +
                                                                              "FROM `student`");) {

            try(ResultSet result = preparedStatement.executeQuery();) {
                while( result.next() )
                    list.add(parseStudent(result));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    // TODO: findById
    // TODO: findByName

    ////////////////////////////////////////
    /// \return Student.
    ///
    public static Student findByUserNameAndPassword(String userName, String password) {
        ConnectionDB connectionDB = new ConnectionDB();
        Student      student      = null;

        try(Connection        connection  = connectionDB.getConnection();
            PreparedStatement statement   = connection.prepareStatement("SELECT `id`, `name`, `last_name`, `user_name`, `password` " +
                                                                        "FROM `student` " +
                                                                        "WHERE `user_name` = ? " +
                                                                        "AND   `password` = ?")) {
            statement.setString(1, userName);
            statement.setString(2, password);

            try(ResultSet result = statement.executeQuery()) {
                if( result.next() )
                    student = parseStudent(result);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return student;
    }

    // TODO: insert
    // TODO: update
    // TODO: delete
}
