package org.silent.university_library.dao;

////////////////////////////////////////////////////////////////////////////////
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.silent.university_library.commun.ConnectionDB;
import org.silent.university_library.commun.DBQueryMaker;
import org.silent.university_library.entity.Book;
////////////////////////////////////////////////////////////////////////////////

public class BookDAO {
    ////////////////////////////////////////
    private static Book parseBook(ResultSet input) throws SQLException {
        Book book = new Book();

        book.setId( input.getLong(Book.COLUMN_ID) );
        book.setTitle( input.getString(Book.COLUMN_TITLE) );
        book.setDescription( input.getString(Book.COLUMN_DESCRIPTION) );
        book.setImage( input.getBytes(Book.COLUMN_IMAGE) );
        book.setCategoryId(input.getLong(Book.COLUMN_CATEGORY_ID));

        return book;
    }

    ////////////////////////////////////////
    /*
    private static Book parseBook(ResultSet input, String[] columns) throws SQLException {
        Book book = new Book();

        for(String column : columns) {
            Book.COLUMNS.get(column).set(book, input.getObject(column));
        }

        return book;
    }
    */

    ////////////////////////////////////////
    public static List<Book> findAll() {
        List<Book>   books = new ArrayList<>();

        try(Connection        connection = ConnectionDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement("SELECT " + Book.COLUMNS_NAMES + " " +
                                                                       "FROM `book` b");) {
            try(ResultSet result = statement.executeQuery();) {
                while( result.next() )
                    books.add(parseBook(result));
            }
        }
        catch(Exception e) {
            System.err.println(e.getMessage());
        }

        return books;
    }

    ////////////////////////////////////////
    public static List<Book> find(DBQueryMaker query) {
        List<Book> books = new ArrayList<>();

        try(Connection connection = ConnectionDB.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT " + Book.COLUMNS_NAMES + " " +
                                                                      "FROM `book` b " +
                                                                      "WHERE " + query.query);) {
            for(int i = 0; i < query.params.size(); i++)
                statement.setObject(i + 1, query.params.get(i));

            try(ResultSet result = statement.executeQuery();) {
                while( result.next() )
                    books.add(parseBook(result));
            }
        }
        catch(Exception e) {
            System.err.println(e.getMessage());
        }

        return books;
    }

    ////////////////////////////////////////
    public static List<Book> find(String criterea, List<Object> values) {
        List<Book> books = new ArrayList<>();

        try(Connection connection = ConnectionDB.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT " + Book.COLUMNS_NAMES + " " +
                                                                      "FROM `book` b " +
                                                                      "WHERE " + criterea);) {
            for(int i = 0; i < values.size(); i++)
                statement.setObject(i + 1, values.get(i));

            try(ResultSet result = statement.executeQuery();) {
                while( result.next() )
                    books.add(parseBook(result));
            }
        }
        catch(Exception e) {
            System.err.println(e.getMessage());
        }

        return books;
    }

    ////////////////////////////////////////
    public static Book load(long id) {
        Book book = null;

        try(Connection connection = ConnectionDB.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT " + Book.COLUMNS_NAMES + " " +
                                                                      "FROM `book` b " +
                                                                      "WHERE b.`id` = ? ");) {
            statement.setLong(1, id);
            try(ResultSet result = statement.executeQuery();) {
                if( result.next() )
                    book = parseBook(result);
            }
        }
        catch(Exception e) {
            System.err.println(e.getMessage());
        }

        return book;
    }

}
