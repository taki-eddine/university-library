package org.silent.university_library.dao;

////////////////////////////////////////////////////////////////////////////////
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import org.silent.university_library.commun.ConnectionDB;
import org.silent.university_library.entity.Category;
////////////////////////////////////////////////////////////////////////////////

public class CategoryDAO {

    ////////////////////////////////////////
    public static Category parseResultSet(ResultSet input) throws Exception {
        Category category = new Category();

        category.setId(input.getLong("id"));
        category.setName(input.getString("name"));

        return category;
    }

    ////////////////////////////////////////
    public static List<Category> findAll() {
        List<Category> categorys = new ArrayList<>();

        try(Connection        connection = ConnectionDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement("SELECT `id`, `name` " +
                                                                       "FROM `category` c ");) {
            try(ResultSet result = statement.executeQuery();) {
                while( result.next() )
                    categorys.add(parseResultSet(result));
            }
        }
        catch(Exception e) {
            System.err.println(e.getMessage());
        }

        return categorys;
    }

}
