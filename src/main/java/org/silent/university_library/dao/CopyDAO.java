package org.silent.university_library.dao;

////////////////////////////////////////////////////////////////////////////////
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.silent.university_library.commun.ConnectionDB;
import org.silent.university_library.entity.Copy;
////////////////////////////////////////////////////////////////////////////////

public class CopyDAO {

    ////////////////////////////////////////
    private static Copy parseResultSet(ResultSet input) throws Exception {
        Copy copy = new Copy();

        copy.setId( input.getLong("id") );
        copy.setDisponible( input.getBoolean("disponible"));

        return copy;
    }

    ////////////////////////////////////////
    public static List<Copy> findByBookId(long bookId) {
        List<Copy> copys = new ArrayList<>();

        try(Connection        connection = ConnectionDB.getConnection();
            PreparedStatement statement  = connection.prepareStatement("SELECT `id`, `disponible`, `book_id`, `type_id` " +
                                                                       "FROM `copy` c " +
                                                                       "WHERE c.`book_id` = ?");) {
            statement.setLong(1, bookId);

            try(ResultSet result = statement.executeQuery();) {
                while( result.next() )
                    copys.add(parseResultSet(result));
            }
        }
        catch(Exception e) {
            System.err.println(e.getMessage());
        }

        return copys;
    }

}
