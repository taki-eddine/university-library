package org.silent.university_library.dao;

////////////////////////////////////////////////////////////////////////////////
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.silent.university_library.commun.ConnectionDB;
import org.silent.university_library.entity.Type;
////////////////////////////////////////////////////////////////////////////////

public class TypeDAO {

    ////////////////////////////////////////
    public static List<Type> findAll() {
        List<Type> types = new ArrayList<>();

        try(Connection connection = ConnectionDB.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT `id`, `name` " +
                                                                      "FROM `type` t");) {
            try(ResultSet result = statement.executeQuery();) {
                while( result.next() )
                    types.add(Type.parseType(result));
            }
        }
        catch(Exception e) {
            System.err.println(e.getMessage());
        }

        return types;
    }
}
