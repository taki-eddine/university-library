package org.silent.university_library.bean;

////////////////////////////////////////////////////////////////////////////////
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import lombok.Getter;
import lombok.Setter;
import org.silent.university_library.commun.DBQueryMaker;
import org.silent.university_library.dao.BookDAO;
import org.silent.university_library.dao.CategoryDAO;
import org.silent.university_library.dao.TypeDAO;
import org.silent.university_library.entity.Book;
import org.silent.university_library.entity.Category;
import org.silent.university_library.entity.Type;
////////////////////////////////////////////////////////////////////////////////

@ManagedBean
@SessionScoped
public class Search {

    @ManagedProperty(value = "#{welcome.searchQuery}")
    @Getter @Setter private String searchQuery = null;

    @Getter @Setter private List<Category>   categories       = null;
    @Getter @Setter private List<SelectItem> categoriesItems  = null;
    @Getter @Setter private int              categoryPosition = 0;

    @Getter @Setter private List<String>     states        = null;
    @Getter @Setter private List<SelectItem> statesItems   = null;
    @Getter @Setter private int              statePosition = 0;

    @Getter @Setter private List<Type>       types        = null;
    @Getter @Setter private List<SelectItem> typesItems   = null;
    @Getter @Setter private int              typePosition = 0;

    @Getter @Setter List<Book> books = null;

    ////////////////////////////////////////
    @PostConstruct
    public void init() {
        DBQueryMaker query = new DBQueryMaker();

        // init books.
        query.like("b.title", searchQuery + "%");
        books = BookDAO.find(query);

        // init states.
        states = new ArrayList<>();
        states.add("Disponible");
        statesItems = new ArrayList<>();

        for(int i = 0; i != states.size(); ++i)
            statesItems.add(new SelectItem(i, states.get(i)));

        // init types.
        types      = TypeDAO.findAll();
        typesItems = new ArrayList<>();

        for(int i = 0; i != types.size(); ++i)
            typesItems.add(new SelectItem(i, types.get(i).getName()));

        // init categories.
        categories      = CategoryDAO.findAll();
        categoriesItems = new ArrayList<>();

        for(int i = 0; i != categories.size(); ++i)
            categoriesItems.add(new SelectItem(i, categories.get(i).getName()));
    }

    ////////////////////////////////////////
    public void search() {
        DBQueryMaker query = new DBQueryMaker();

        query.like("b.title", searchQuery + "%");
        books = BookDAO.find(query);

        if( categoryPosition != -1 )
            books.removeIf((book) -> {
                return book.getCategoryId() != categories.get(categoryPosition).getId();
            });

        if( statePosition != -1 )
            books.removeIf((book) -> {
                return book.getOwnCopys().isEmpty();
            });
    }

}
