package org.silent.university_library.bean;

////////////////////////////////////////////////////////////////////////////////
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.silent.university_library.commun.SessionHandler;
import org.silent.university_library.dao.StudentDAO;
import org.silent.university_library.entity.Student;
////////////////////////////////////////////////////////////////////////////////

@ManagedBean
@SessionScoped
public class Login implements Serializable {

    @Getter @Setter private String userName = null;
    @Getter @Setter private String password = null;

    ////////////////////////////////////////
    /// @return List of students.
    ///
    public List<Student> getStudents() {
        return StudentDAO.findAll();
    }

    ////////////////////////////////////////
    public String verify() {
        Student student  = StudentDAO.findByUserNameAndPassword(userName, password);
        String  redirect = null;

        if( student == null ) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error !", "User Name or Password is wrong."));
            redirect = "login";
        }
        else {
            SessionHandler.save("id", student.getId());
            SessionHandler.save("user_name", student.getUserName());
            redirect = "welcome";
        }

        return redirect;
    }

}
