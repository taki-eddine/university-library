package org.silent.university_library.bean;

////////////////////////////////////////////////////////////////////////////////
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import org.silent.university_library.commun.SessionHandler;
import org.silent.university_library.dao.BookDAO;
import org.silent.university_library.entity.Book;
////////////////////////////////////////////////////////////////////////////////

@ManagedBean
@SessionScoped
public class Home {

    @Getter private String userName = SessionHandler.load("user_name");

    ////////////////////////////////////////
    public List<Book> getBooks() {
        return BookDAO.findAll();
    }

    ////////////////////////////////////////
    public String logout() {
        SessionHandler.invalidate();
        return "login";
    }

}
