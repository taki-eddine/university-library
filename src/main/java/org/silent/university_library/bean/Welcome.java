package org.silent.university_library.bean;

////////////////////////////////////////////////////////////////////////////////
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import lombok.Getter;
import lombok.Setter;
////////////////////////////////////////////////////////////////////////////////

@ManagedBean
@SessionScoped
public class Welcome implements Serializable {

    @Getter @Setter private String searchQuery = null;

    ////////////////////////////////////////
    public String go() {
        return "search";
    }
}
