package org.silent.university_library.bean.service;

////////////////////////////////////////////////////////////////////////////////
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.silent.university_library.dao.BookDAO;
import org.silent.university_library.entity.Book;
////////////////////////////////////////////////////////////////////////////////

@ManagedBean
@ApplicationScoped
public class BookService {

    ////////////////////////////////////////
    public StreamedContent getImage() throws IOException {
        FacesContext    context = FacesContext.getCurrentInstance();
        StreamedContent content = null;
        String          bookId  = null;
        Book            book    = null;

        if( context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE ) {
            // We're rendering the HTML.
            content = new DefaultStreamedContent();
        }
        else {
            // The browser browser is requesting the image.
            bookId = context.getExternalContext().getRequestParameterMap().get("bookId");
            book   = BookDAO.load(Long.parseLong(bookId));
            content = new DefaultStreamedContent(new ByteArrayInputStream(book.getImage()));
        }

        return content;
    }

}
