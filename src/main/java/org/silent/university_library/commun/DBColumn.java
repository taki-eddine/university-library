package org.silent.university_library.commun;

////////////////////////////////////////////////////////////////////////////////
import java.lang.reflect.ParameterizedType;
import java.util.function.Function;
import javafx.util.Pair;
////////////////////////////////////////////////////////////////////////////////

public class DBColumn<B extends Object, T extends Object> {

    public Class<T> type;

    ////////////////////////////////////////
    public DBColumn(Function<Pair<B, T>, Void> set) {
        this.type = getClassOfT();
        this.set  = set;
    }

    ////////////////////////////////////////
    public void set(B object, T value) {
        set.apply(new Pair<>(object, value));
    }

//----------------------------------------------------------------------------//
    private Function<Pair<B, T>, Void> set;

    ////////////////////////////////////////
    private Class<T> getClassOfT() {
        return (Class<T>)((ParameterizedType)DBColumn.class.getGenericSuperclass()).getActualTypeArguments()[1];
    }

}
