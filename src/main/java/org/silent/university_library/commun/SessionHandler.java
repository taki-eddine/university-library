package org.silent.university_library.commun;

////////////////////////////////////////////////////////////////////////////////
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
////////////////////////////////////////////////////////////////////////////////

public class SessionHandler {

    ////////////////////////////////////////
    public static HttpSession getSession() {
        return (HttpSession)FacesContext.getCurrentInstance()
                                        .getExternalContext()
                                        .getSession(false);
    }

    ////////////////////////////////////////
    public static HttpServletRequest getRequest() {
        return (HttpServletRequest)FacesContext.getCurrentInstance()
                                               .getExternalContext()
                                               .getRequest();
    }

    ////////////////////////////////////////
    public static boolean has(String key) {
        HttpSession session = getSession();
        boolean result = false;

        if( session != null )
            result = session.getAttribute(key) != null;

        return result;
    }

    ////////////////////////////////////////
    public static void save(String key, String value) {
        HttpSession session = getSession();
        session.setAttribute(key, value);
    }

    ////////////////////////////////////////
    public static void save(String key, int value) {
        HttpSession session = getSession();
        session.setAttribute(key, value);
    }

    ////////////////////////////////////////
    public static String load(String key) {
        HttpSession session = getSession();
        return (String)session.getAttribute(key);
    }

    ////////////////////////////////////////
    public static void invalidate() {
        HttpSession session = getSession();
        session.invalidate();
    }

}
