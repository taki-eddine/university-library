package org.silent.university_library.commun;

////////////////////////////////////////////////////////////////////////////////
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
////////////////////////////////////////////////////////////////////////////////

public class ConnectionDB {
    // database settings
    final static String URL    = "jdbc:mysql://127.0.0.1:3306/university_library";
    final static String DRIVER = "com.mysql.jdbc.Driver";
    final static String NAME   = "root";
    final static String PWD    = "";

    //@Resource(name = "jdbc/university_library") // resource injection.
    //private DataSource dataSource = null;

    ////////////////////////////////////////
    public ConnectionDB() {
        /*try {
            //Context context = new InitialContext();
            //dataSource = (DataSource)context.lookup("java:comp/env/jdbc/university_library");
        }
        catch(NamingException e) {
            e.printStackTrace();
        }*/
    }

    /*
    ////////////////////////////////////////
    public Connection getConnection() throws Exception {
        /*if( dataSource == null )
            throw new SQLException("Can't get data source");
        return dataSource.getConnection();* /

        DriverManager.registerDriver((Driver)Class.forName(DRIVER).newInstance());
        return DriverManager.getConnection(URL, NAME, PWD);
    }
    */

    ////////////////////////////////////////
    public static Connection getConnection() throws Exception {
        DriverManager.registerDriver((Driver)Class.forName(DRIVER).newInstance());
        return DriverManager.getConnection(URL, NAME, PWD);
    }
}
