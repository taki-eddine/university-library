package org.silent.university_library.commun;

////////////////////////////////////////////////////////////////////////////////
import java.util.ArrayList;
import java.util.List;
////////////////////////////////////////////////////////////////////////////////

public class DBQueryMaker {

    public String       query  = "";
    public List<Object> params = new ArrayList<>();

    ////////////////////////////////////////
    public DBQueryMaker clear() {
        query = "";
        params.clear();
        return this;
    }

    ////////////////////////////////////////
    public DBQueryMaker like(String column, Object param) {
        query  += column + " LIKE ?";
        params.add(param);
        return this;
    }

    ////////////////////////////////////////
    public DBQueryMaker eq(String column, Object param) {
        query  += column + " = ?";
        params.add(param);
        return this;
    }

    ////////////////////////////////////////
    public DBQueryMaker and() {
        query += " AND ";
        return this;
    }

    ////////////////////////////////////////
    public DBQueryMaker or() {
        query += " OR ";
        return this;
    }

}
