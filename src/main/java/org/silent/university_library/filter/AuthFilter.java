package org.silent.university_library.filter;

////////////////////////////////////////////////////////////////////////////////
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
////////////////////////////////////////////////////////////////////////////////

@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml" })
public class AuthFilter implements Filter {

    ////////////////////////////////////////
    public AuthFilter() {
    }

    ////////////////////////////////////////
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    ////////////////////////////////////////
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // check whether session variable is set.
        HttpServletRequest  req     = (HttpServletRequest)request;
        HttpServletResponse res     = (HttpServletResponse)response;
        HttpSession         session = req.getSession(false);
        String              uri     = req.getRequestURI();

        if( (!uri.contains("/login.xhtml") && (session != null && session.getAttribute("user_name") != null))
           || (uri.contains("/login.xhtml") && !(session != null && session.getAttribute("user_name") != null)) )
            chain.doFilter(request, response);
        else if( uri.contains("/login.xhtml") && (session != null && session.getAttribute("user_name") != null) )
            res.sendRedirect(req.getContextPath() + "/faces/welcome.xhtml");
        else
            res.sendRedirect(req.getContextPath() + "/faces/login.xhtml");
    }

    ////////////////////////////////////////
    @Override
    public void destroy() {
    }

}
